# Android (for React Native, etc.)
# See http://facebook.github.io/react-native/docs/getting-started.html
# https://reactnative.dev/docs/getting-started.html
# https://github.com/facebook/react-native/wiki/Building-from-source
# https://developer.android.com/studio/command-line/variables
export ANDROID_HOME=~/Library/Android/sdk
export ANDROID_SDK_ROOT=~/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools
